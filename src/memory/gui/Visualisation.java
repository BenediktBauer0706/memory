package memory.gui;

import memory.Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Visualisation extends JFrame {

  private static final int CELL_SIZE = 128;

  private final Game game;
  private final int  size;

  private int[]       card1;
  private int[]       card2;
  private boolean[][] found;
  private boolean     isPaused;

  public Visualisation () {

    Dimension screenSize = Toolkit.getDefaultToolkit ().getScreenSize ();
    int y = (screenSize.height - 75) / CELL_SIZE;
    int x = (screenSize.width - 75) / CELL_SIZE;
    size = Math.min (x, y);

    game = new Game (size);
    found = new boolean[size][size];

    card1 = new int[] {-1, -1, -1};
    card2 = new int[] {-1, -1, -1};

    setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    setLayout (new BorderLayout ());

    JPanel vPanel = createVPanel ();
    add (vPanel, BorderLayout.CENTER);
    pack ();

    setLocationRelativeTo (null);
    setResizable (false);
    setTitle ("Memory for 2");
    setVisible (true);
  }

  private JPanel createVPanel () {
    JPanel visualisationPanel = new JPanel (new BorderLayout ()) {

      @Override
      protected void paintComponent (Graphics g) {
        super.paintComponent (g);
        drawGrid (g);
      }
    };

    visualisationPanel.addMouseListener (new MouseAdapter () {

                                           int count = 0;

                                           @Override
                                           public void mouseClicked (MouseEvent e) {
                                             int x = e.getX () / CELL_SIZE;
                                             int y = e.getY () / CELL_SIZE;
                                             if (!isPaused) {
                                               if (count == 0 && !found[y][x]) {
                                                 game.setCard1 (x, y);
                                                 card1 = new int[] {x, y, game.getCard1 ()};
                                                 ++count;
                                                 repaint ();
                                               } else if (count == 1) {
                                                 if ((x != card1[0] || y != card1[1]) && !found[y][x]) {
                                                   game.setCard2 (x, y);
                                                   card2 = new int[] {x, y, game.getCard2 ()};
                                                   if (game.pick ()) {
                                                     found[card1[1]][card1[0]] = true;
                                                     found[card2[1]][card2[0]] = true;
                                                   }
                                                   count = 0;
                                                   repaint ();
                                                   pause ();
                                                 }

                                               }
                                             }

                                           }

                                         }

    );

    int size = this.size * CELL_SIZE;

    visualisationPanel.setPreferredSize (new Dimension (size, size));

    return visualisationPanel;
  }

  private void drawGrid (Graphics g) {

    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        int x = j * CELL_SIZE;
        int y = i * CELL_SIZE;
        if (found[i][j]) {
          g.setColor (Color.WHITE);
        } else {
          g.setColor (Color.LIGHT_GRAY);
        }
        g.fillRect (x, y, CELL_SIZE, CELL_SIZE);
        g.setColor (Color.BLACK);
        g.drawRect (x, y, CELL_SIZE, CELL_SIZE);
      }
    }
    drawCard (g, card1[0], card1[1], card1[2]);
    drawCard (g, card2[0], card2[1], card2[2]);
  }

  private void drawCard (Graphics g,
                         int x,
                         int y,
                         int value) {
    if (x == -1 || y == -1 || value <= 0) {
      return;
    }

    int _x = x * CELL_SIZE;
    int _y = y * CELL_SIZE;
    g.setColor (getCellColor (value));
    g.fillRect (_x, _y, CELL_SIZE, CELL_SIZE);
    g.setColor (Color.BLACK);
    g.drawRect (_x, _y, CELL_SIZE, CELL_SIZE);
    g.drawString ("" + value, _x, _y + CELL_SIZE);
  }

  private Color getCellColor (int value) {
    double normalizedValue = (double) value / size * size / 2;

    return rainbowColor (normalizedValue);
  }

  private Color rainbowColor (double value) {
    float hue = (float) (value * 0.7);
    return Color.getHSBColor (hue, 1.0f, 1.0f);
  }

  private void pause () {
    isPaused = true;
    Timer timer = new Timer (1500, e -> {
      if (game.isSolved ()) {
        JOptionPane.showMessageDialog (null, game.getWinner ());
        found = new boolean[size][size];
      }
      setTitle ();
      card1 = new int[] {-1, -1, -1};
      card2 = new int[] {-1, -1, -1};
      repaint ();
      isPaused = false;
    });
    timer.setRepeats (false);
    timer.start ();

  }

  private void setTitle () {
    setTitle ("Turn: " + (game.isP1Turn () ? "Player 1" : "Player 2"));
  }
}
