package memory;

public class Game {

  private final Memory memory;

  private int     card1;
  private int     card2;
  private int     p1_score;
  private int     p2_score;
  private boolean isP1Turn;

  public Game (int size) {
    memory = new Memory (size);
    memory.shuffle ();
    p1_score = 0;
    p2_score = 0;
    isP1Turn = true;
  }

  public boolean pick () {

    if (card1 == -1 || card2 == -1) {
      System.out.println ("no selection made");
      return false;
    }

    if (checkPair (card1, card2)) {
      if (isP1Turn) {
        p1_score++;
      } else {
        p2_score++;
      }
      memory.remove (card1);
      card1 = -1;
      card2 = -1;
      return true;
    } else {
      isP1Turn = !isP1Turn;
      card1 = -1;
      card2 = -1;
      return false;
    }
  }

  public void setCard1 (int x,
                        int y) {
    card1 = memory.select (x, y);
  }

  public void setCard2 (int x,
                        int y) {
    card2 = memory.select (x, y);
  }

  public int getCard1 () {
    return card1;
  }

  public int getCard2 () {
    return card2;
  }

  public boolean checkPair (int c1,
                            int c2) {
    return c1 == c2 && c1 > 0;
  }

  public boolean isSolved () {
    return memory.isSolved ();
  }

  public String getWinner () {
    String end;
    if (p1_score == p2_score) {
      end = "Draw";
      System.out.println (end);
    } else {
      end = (p1_score > p2_score ? " P1 " : " P2 ") + "won. " + p1_score + ":" + p2_score;
      System.out.println (end);
    }
    memory.shuffle ();
    isP1Turn = p2_score >= p1_score;
    p1_score = 0;
    p2_score = 0;
    return end;
  }

  public boolean isP1Turn () {
    return isP1Turn;
  }
}
