package memory;

import java.util.Arrays;
import java.util.Random;

public class Memory {

  private static final int MIN_SIZE = 2;
  private static final int MAX_SIZE = 32;

  private final int[][] board;

  public Memory (int size) {
    if (size > MAX_SIZE || size < MIN_SIZE) {
      throw new IllegalArgumentException (String.format ("size oob, bounds [%d,%d]", MIN_SIZE, MAX_SIZE));
    }

    board = new int[size][size];
    shuffle ();
  }

  private void initBoard () {
    int number = 1;
    int count = 0;
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[i].length; j++) {
        if (count == 2) {
          ++number;
          count = 0;
        }
        board[i][j] = number;
        count++;
      }
    }
  }

  public void shuffle () {
    if (isSolved ()) {
      initBoard ();
    }
    Random rand = new Random ();
    int n = board.length;
    for (int i = n * n - 1; i > 0; i--) {
      int j = rand.nextInt (i + 1);
      swap (i / n, i % n, j / n, j % n);
    }
  }

  private void swap (int i1,
                     int j1,
                     int i2,
                     int j2) {
    int temp = board[i1][j1];
    board[i1][j1] = board[i2][j2];
    board[i2][j2] = temp;
  }

  public int select (int x,
                     int y) {
    if (x > board.length || y > board.length || x < 0 || y < 0) {
      System.out.println ("selection oob");
      return -1;
    }
    return board[y][x];
  }

  public void remove (int cardValue) {
    Arrays.stream (board).forEach (row -> Arrays.setAll (row, j -> row[j] == cardValue ? 0 : row[j]));
  }

  public boolean isSolved () {
    return Arrays.stream (board).flatMapToInt (Arrays::stream).sum () == 0;
  }

}
